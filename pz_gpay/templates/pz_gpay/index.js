import { CheckoutService } from 'pz-core';

const trans = {
  error: gettext('An error occured, please try again later.'),
};

class GPayOption {
  constructor(option, customSettings = {}) {
    this.paymentOption = option;

    const settings = {
      formSelector: '.js-gpay-form',
      agreementInputSelector: '.js-checkout-agreement-input',
      agreementErrorSelector: '.js-agreement-error',
      formErrorsSelector: '.js-form-errors',
      ...customSettings
    };

    this.form = document.querySelector(settings.formSelector);
    this.agreementCheckbox = document.querySelector(settings.agreementInputSelector);
    this.agreementError = document.querySelector(settings.agreementErrorSelector);
    this.errors = document.querySelector(settings.formErrorsSelector);

    this.agreementCheckbox.validations[0].errorPlacement = this.agreementError;

    this.bindEvents();
  }

  onActionSuccess = (response) => {
    const { data } = response.detail.response;
    const redirectUrl = data.context_list?.[0]?.page_context?.redirect_url;

    if (!redirectUrl) {
      this.onError();
      return;
    }

    window.location = redirectUrl;
  }

  onActionError = () => { this.onError(); }

  onValidationChange = (response) => { this.agreementError.hidden = response.detail.valid; }

  bindEvents = () => {
    this.form.beforeAction = async () => {
      this.errors.innerHTML = '';
      this.errors.setAttribute('hidden', true);

      await CheckoutService.setGPay();
    };

    this.form.addEventListener('action-success', this.onActionSuccess);
    this.form.addEventListener('action-error', this.onActionError);
    this.agreementCheckbox.addEventListener('validation-change', this.onValidationChange);
  }

  onError = () => {
    this.errors.innerHTML = trans.error;
    this.errors.removeAttribute('hidden');
  }

  unmount = () => {
    this.agreementCheckbox.removeEventListener('validation-change', this.onValidationChange);
    this.form.removeEventListener('action-success', this.onActionSuccess);
    this.form.removeEventListener('action-error', this.onActionError);
  };
}

export default GPayOption;
