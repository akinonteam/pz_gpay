# pz_gpay

A library module for enabling "Garanti Pay" payment module at order/payment step.

## Usage

### 1. Install the app

At project root, create a requirements.txt if it doesn't exist and add the following line to it;

```bash

-e git+https://git@bitbucket.org/akinonteam/pz_gpay.git@89b9683

```

For more information about this syntax, check [pip install docs](https://pip.pypa.io/en/stable/reference/pip_install/#git).

Next, run the following command to install the package.

```bash
# in venv
pip install -r requirements.txt
```

### 2. Install the npm package

```bash

# in /templates

yarn add git+ssh://git@bitbucket.org:akinonteam/pz_gpay.git#89b9683

```

Make sure to use the same git commit id as in `requirements.txt`.

### 3. Add to the project

```python
# omnife_base/settings.py:
INSTALLED_APPS.append('pz_gpay')
```

### 4. Import template:

```jinja
{# in templates/orders/checkout/payment/index.html #}
{% from 'pz_gpay/index.html' import GPayForm %}

{# example using scenario at tabs structure #}

{# should use where there are tab buttons #}
<button class="checkout-payment-type__button js-payment-tab" data-type="gpay" hidden>
  {{ _('GPay') }}
</button>

{# should use where there are tab contents #}
<div class="js-payment-tab-content" data-type="gpay" hidden>
  {{ GPayForm() }}
</div>
```

### 5. Import and initialize JS

```js
// in templates/orders/checkout/payment/index.js
import GPayOption from 'pz-gpay';

class PaymentTab {
  // ...
  activePaymentOptions = [
    // other payment options ...
    {
      type: 'gpay',
      handler: GPayOption
    },
  ];
  // ...
}

// Or pass an object, which will be taken as destructured parameters, to customize things on the JS class.
class PaymentTab {
  // ...
  activePaymentOptions = [
    // other payment options ...
    {
      type: 'gpay',
      handler: GPayOption,
      settings: {
        formSelector: '.js-gpay-form',
        agreementInputSelector: '.js-checkout-agreement-input',
        agreementErrorSelector: '.js-agreement-error',
        formErrorsSelector: '.js-form-errors',
      }
    },
  ];
  // ...
}
```

All of the followings are optional parameters for the GPayOption config.  

- **formSelector**: (String)
- **agreementInputSelector**: (String)
- **agreementErrorSelector**: (String)
- **formErrorsSelector**: (String)

## Customizing the Html (Jinja Macro)

```jinja
{# Simple Using #}
{{ GPayForm() }}

{# Using w/ Custom Template #}
{% call GPayForm() %}
  {# ...custom html elements... #}
{% endcall %}

{# Simple w/ Passing Parameters (Default Parameters) #}
{{ GPayForm(
  service="Checkout",
  action="completeGPay",
  title=_('GarantiPay Info'),
  description=None,
  submitButtonText=_('Pay with GarantiPay')
) }}
```

All of the followings are optional parameters for the Jinja2 macro.  

- **service**: (String)
- **action**: (String)
- **title**: (String / Gettext)
- **description**: (String / Gettext)
- **submitButtonText**: (String / Gettext)
